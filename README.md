# Tree of Life

I started this project in middle school, c. 2015, to create a complete tree of life. I started out by using Microsoft Excel, and that version is here in the repo as `TreeOfLife.xlsx`. Parts of it may be outdated and I'm not sure if I'll ever complete that one. It does also contain a more detailed phylogeny of extant mammals though (on a separate sheet in the same file), which is useful.

My ultimate goal, as of now, is to make a hand-drawn tree of life, with time going left to right, to scale, and thickness of the lines representing total dry biomass. This image would need to be extremely large, and art software on my laptop probably could not handle it. I have started a version of this (to be committed later) but the resolution is far too low to actually get down to species.
